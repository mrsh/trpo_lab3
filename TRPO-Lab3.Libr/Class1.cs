﻿using System;

namespace TRPO_Lab3.Libr
{
    public class Profit
    {
        public static float Gera(float a, float c, float b)
        {
            if (Error(a,b,c))
            {
                throw new ArgumentException("Зочем отрицательное?");
            }
            else
            {
                float P = (a + b + c) / 2;
                return (float)Math.Sqrt(P * (P - a) * (P - b) * (P - c));
            }
        }
        static bool Error(float a, float b, float c)
        {
            if (a < 0 || b<0 || c<0) return true;
            else return false;
        }
    }



}

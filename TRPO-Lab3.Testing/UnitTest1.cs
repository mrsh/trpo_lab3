using NUnit.Framework;
using TRPO_Lab3;
using static TRPO_Lab3.Libr.Profit;
using System;

namespace TRPO_Lab3.Testing
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            const int a = 3;
            const int b = 4;
            const int c = 5;
            const int expresult = 6;          
            Assert.AreEqual(Gera(a, b, c), expresult);
        }
        [Test]
        public void Test2()
        {
            const int a = 4;
            const int b = -3;
            const int c = 5;

            Assert.Throws<ArgumentException>(() => Gera(a, b,c));
        }
    }
}